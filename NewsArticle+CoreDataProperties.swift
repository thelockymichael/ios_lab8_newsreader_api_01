//
//  NewsArticle+CoreDataProperties.swift
//  iOS_Lab8_NewsReader_API_01
//
//  Created by Micky on 18.11.2020.
//
//

import Foundation
import CoreData


extension NewsArticle {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NewsArticle> {
        return NSFetchRequest<NewsArticle>(entityName: "NewsArticle")
    }

    @NSManaged public var title: String?
    @NSManaged public var author: String?
    @NSManaged public var desc: String?
    @NSManaged public var urlToImage: String?
    @NSManaged public var publishedAt: String?
    @NSManaged public var content: String?
    @NSManaged public var url: String?

}

extension NewsArticle : Identifiable {

}
