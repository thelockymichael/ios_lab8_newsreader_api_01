//
//  ViewController.swift
//  iOS_Lab8_NewsReader_API_01
//
//  Created by Micky on 17.11.2020.
//

import UIKit
import Alamofire
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var searchController: UISearchBar!
    
    var articles: [NewsArticle] = []
    
    var selectedItem: NewsArticle?
    
    @IBAction func fetchNewsAction(_ sender: Any) {
        fetchArticles()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView,
                   willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedItem = articles[indexPath.row]
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ArticleTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ArticleTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        // Set TableViewCell UI element values
        
        let item = articles[indexPath.row]
    
        let url = URL(string: item.urlToImage ?? "")!
        
        DispatchQueue.global().async {
            // Fetch Image Data
            if let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    // Create Image and Update Image View
                    cell.articleImage.image = UIImage(data: data)
                }
            }
        }
        
        cell.titleLabel.text = item.title
        
        return cell
    }
    
    func fetchArticles() {
        // Query Bitcoin related news
        let request =
            AF.request("https://newsapi.org/v2/everything?q=bitcoin&from=2020-10-17&sortBy=publishedAt&apiKey=b5c917cf50a64f0fae138c6a424e893e")
        
        request
            .validate()
            .responseDecodable(of: Articles.self) {
                response in
                guard let articles = response.value else { return }
                
                // Iterates fetched articles and saves them individually
                for article in articles.all {
                    let newArticle = NewsArticle(context: self.context)
                    newArticle.title = article.title ?? "no title"
                    newArticle.url = article.url ?? "no url"
                    newArticle.urlToImage = article.urlToImage ?? "no url to image"
                }
                
                // Save the data
                do {
                    try self.context.save()
                } catch {
                    fatalError("Could not fetch news articles.")
                }
                
                self.fetchSavedNewsArticles()
            
            }
    }
    
    // Called in viewDidLoad() method at start
    func fetchSavedNewsArticles() {
        do {
            self.articles = try context.fetch(NewsArticle.fetchRequest())
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch {
            fatalError("You died.")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        if (segue.identifier == "toDetailVC") {
            guard let destinationVC = segue.destination as? DetailViewController else {
                return
            }
            
            destinationVC.data = selectedItem
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.dataSource = self
        tableView.delegate = self
        
        searchController.delegate = self
        
        searchController.showsCancelButton = true
        
        fetchSavedNewsArticles()
    }
}

// MARK: - UISearchBarDelegate
extension ViewController: UISearchBarDelegate {
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    
    guard let searchText = searchController.text else { return }
    
    do {
        let request = NewsArticle.fetchRequest() as NSFetchRequest<NewsArticle>
        
        // Set the filtering and sorting on the request
        // Filters news by title
         let pred = NSPredicate(format: "title CONTAINS %@", searchText)
         request.predicate = pred
        
        self.articles = try context.fetch(request)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    } catch {
        fatalError("You died.")
    }

  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchController.text = nil
    searchController.resignFirstResponder()
    
    self.fetchSavedNewsArticles()
    
    DispatchQueue.main.async {
        self.tableView.reloadData()
    }
  }
}


