//
//  Articles.swift
//  iOS_Lab8_NewsReader_API_01
//
//  Created by Micky on 17.11.2020.
//

import Foundation

struct Articles: Decodable {
    let count: Int
    let all: [Article]
    
    enum CodingKeys: String, CodingKey {
        case count = "totalResults"
        case all = "articles"
    }
}
