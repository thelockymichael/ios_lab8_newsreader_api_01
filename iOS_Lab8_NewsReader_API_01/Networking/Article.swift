//
//  Article.swift
//  iOS_Lab8_NewsReader_API_01
//
//  Created by Micky on 17.11.2020.
//

import Foundation

struct Article: Decodable {
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?
    
    enum CodingKeys: String, CodingKey {
        case author
        case title
        case description
        case url
        case urlToImage
        case publishedAt
        case content
    }
}
