//
//  DetailViewController.swift
//  iOS_Lab8_NewsReader_API_01
//
//  Created by Micky on 18.11.2020.
//

import UIKit
import WebKit

class DetailViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var web: WKWebView!
    
    var data: NewsArticle?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: data?.url ??  "https://www.google.com")
        web.load(URLRequest(url: url!))
    }
}
